import react from "react";
import styled from "styled-components";
import 'bootstrap/dist/css/bootstrap.min.css';


const Boton = styled.button`
width:40px;
height:40px;
background-color: grey;
border: 3px solid white;
border-radius:10px;
color:white;
text-align:center;
justify-context:center;
display:inline-block;
float:left;
margin-left: 16px;
margin-top:7px;
font-weight: bold;
cursor:pointer;
font-family: 'digital';
box-shadow: 0 8px 16px 0 rgba(0,0,0,0.2), 0 6px 20px 0 rgba(0,0,0,0.19);
`





export default (props) => {

    return (
       <>
            <Boton onClick={ () => props.funcionClicar(props.texto) } > {props.texto} </Boton>
            
        </>

    );
}