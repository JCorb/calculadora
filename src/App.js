import react, {useState} from "react";
import styled from "styled-components";
import 'bootstrap/dist/css/bootstrap.min.css';
import Boton from "./Boton";
import Pantalla from "./Pantalla";

const Fondo = styled.div`
width:220px;
height:310px;
background-color: babyblue;;
border: 5px solid black;
margin:10px;
border-radius:5px;
`
export default () => {

  const [primerValor, setPrimerValor] = useState("");
  const [display, setDisplay] = useState("");
  const [operacion, setOperacion] = useState("");

  const [escribe, setEscribe] = useState("");

  const nums = [0,1,2,3,4,5,6,7,8,9];
  const ops = ["+","-","*","/"]
   
  const clicar = (x) =>{
    console.log(x,display);
    if(nums.includes(x*1)){

    setDisplay(display + x);
    setEscribe(escribe+x);

    }
    else if(ops.includes(x)){

      setPrimerValor(display);
      setOperacion(x);
      setEscribe(escribe+x);
      setDisplay("");
      
    }else if (x =="="){
      if (operacion == "+"){
        setDisplay(primerValor*1+display*1);
        setPrimerValor(primerValor*1+display*1);

      }else if (operacion == "-"){
        setDisplay(primerValor*1-display*1);
        setPrimerValor(primerValor*1-display*1);

      }else if (operacion == "*"){
        setDisplay(primerValor*1*display*1);
        setPrimerValor(primerValor*1*display*1);

      }else if (operacion == "/"){
        setDisplay((primerValor*1)/(display*1));
        setPrimerValor((primerValor*1)/(display*1));

      }

    }else if (x == "C"){
      setDisplay("");
      setEscribe("");
    };

  

};
  
  



  return (

  

    <>
      <Fondo>
        <div className="row">
          <Pantalla valor={display} escribe={escribe}>
          
          </Pantalla>
        </div>

        <div className="row">
          <div className="col=3"><Boton funcionClicar={clicar} texto="1"></Boton></div>
          <div className="col=3"><Boton funcionClicar={clicar} texto="2"></Boton></div>
          <div className="col=3"><Boton funcionClicar={clicar} texto="3"></Boton></div>
          <div className="col=3"><Boton funcionClicar={clicar} texto="*"></Boton></div>
        </div>
        <div className="row">
          <div className="col=3"><Boton funcionClicar={clicar} texto="4"></Boton></div>
          <div className="col=3"><Boton funcionClicar={clicar} texto="5"></Boton></div>
          <div className="col=3"><Boton funcionClicar={clicar} texto="6"></Boton></div>
          <div className="col=3"><Boton funcionClicar={clicar} texto="/"></Boton></div>
        </div>
        <div className="row">
          <div className="col=3"><Boton funcionClicar={clicar} texto="7"></Boton></div>
          <div className="col=3"><Boton funcionClicar={clicar} texto="8"></Boton></div>
          <div className="col=3"><Boton funcionClicar={clicar} texto="9"></Boton></div>
          <div className="col=3"><Boton funcionClicar={clicar} texto="-"></Boton></div>
        </div>
        <div className="row">
          <div className="col=3"><Boton funcionClicar={clicar} texto="0"></Boton></div>
          <div className="col=3"><Boton funcionClicar={clicar} texto="C"></Boton></div>
          <div className="col=3"><Boton funcionClicar={clicar} texto="="></Boton></div>
          <div className="col=3"><Boton funcionClicar={clicar} texto="+"></Boton></div>
        </div></Fondo>

    </>

  );

}