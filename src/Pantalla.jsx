import react from "react";
import styled from "styled-components";
import 'bootstrap/dist/css/bootstrap.min.css';

const Pantalla = styled.div`
    width:300px;
    height:70px;
    background-color: white;
    border: 3px solid grey;
    margin: 15px;
    border-radius:5px;
    text-align:right;
    font-size:30px;
    font: 'digital';
    `

const Proceso = styled.div`
    text-align:left;
    font-size:15px;
    margin-bottom:30px;

    `
export default (props) => {


    return (
        <>
            <Pantalla>
                {props.valor}
                <Proceso> {props.escribe} </Proceso>
            </Pantalla>
        </>
    );



}